using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class GoDownElevatorAction : ElevatorMovementAction
{
    protected override ActionTypes Worker(Elevator elevator, List<Passenger> passengers)
    {
        return ActionTypes.GoDown;
    }

    public GoDownElevatorAction(Elevator elevator, List<Passenger> passengers) : base(elevator, passengers) { }
}
