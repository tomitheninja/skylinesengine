using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class GoUpElevatorAction : ElevatorMovementAction
{
    protected override ActionTypes Worker(Elevator elevator, List<Passenger> passengers)
    {
        return ActionTypes.GoUp;
    }

    public GoUpElevatorAction(Elevator elevator, List<Passenger> passengers) : base(elevator, passengers) { }
}
