using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

public class StopElevatorAction : ElevatorMovementAction
{
    protected override ActionTypes Worker(Elevator elevator, List<Passenger> passengers)
    {
        return ActionTypes.Stop;
    }

    public StopElevatorAction(Elevator elevator, List<Passenger> passengers) : base(elevator, passengers) { }
}
