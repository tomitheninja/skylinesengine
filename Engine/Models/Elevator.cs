using System;
using System.Collections;
using System.Collections.Generic;

/// <summary>The Elevator model represents someone who uses, used or will use the elevator</summary>
public class Elevator : ObjectWithUniqueID
{
    /// <summary>
    /// Log of last movements
    /// </summary>
    public ElevatorMovementAction.ActionTypes[] History = new ElevatorMovementAction.ActionTypes[10];

    /// <summary>My last movement action</summary>
    /// <value></value>
    public ElevatorMovementAction.ActionTypes LastAction
    {
        get
        {
            return History[0];
        }
    }

    /// <summary>
    /// Last action excluding stop
    ///
    /// Defaults to Initial
    /// </summary>
    /// <value></value>
    public ElevatorMovementAction.ActionTypes LastMovement
    {
        get
        {
            foreach (var action in History)
            {
                if (action == ElevatorMovementAction.ActionTypes.GoDown
                || action == ElevatorMovementAction.ActionTypes.GoUp) return action;
            }
            return ElevatorMovementAction.ActionTypes.Initial;
        }
    }

    /// <summary>Check if my doors were opened in the last tick</summary>
    /// <value></value>
    public bool isDoorOpen
    {
        get
        {
            return LastAction == ElevatorMovementAction.ActionTypes.Stop;
        }
    }

    /// <summary>The maximum number of passengers inside me</summary>
    /// <value></value>
    public int MaxCapacity { get; private set; }

    /// <summary>The number of floors.</summary>
    /// <value>Equals the max value of currentFloor + 1</value>
    public int Height { get; private set; }

    /// <summary>Number of passengers inside me</summary>
    /// <value></value>
    public int NumPassengers
    {
        get { return PassengersInside.Length; }
    }

    /// <summary>Array of passengers inside me</summary>
    /// <value></value>
    public Passenger[] PassengersInside { get; private set; }

    /// <summary>The floor which the elevator is on or last passed</summary>
    /// <value>A floor index.</value>
    public int CurrentFloor
    {
        get
        {
            return _currentFloor;
        }
        private set
        {
            if (value >= Height) throw new ArgumentOutOfRangeException("I can't go above the top floor");
            if (value < 0) throw new ArgumentOutOfRangeException("I can't go below 0th floor");
            if (Math.Abs(CurrentFloor - value) != 1) throw new Exception("I can only move one floor per tick");
            _currentFloor = value;
        }
    }

    /// <summary>!! Please use my public API !!</summary>
    private int _currentFloor;

    /// <summary></summary>
    /// <param name="initialHeight">The elevator's initial height.</param>
    public Elevator(int initialHeight = 2)
    {
        Height = initialHeight;
        MaxCapacity = 1;
        PassengersInside = new Passenger[0];
        if (initialHeight < 2) throw new ArgumentOutOfRangeException("I needs at least two floors");
    }

    /// <summary>Increases the number of floors by one</summary>
    public void AddFloor()
    {
        Height++;
    }

    /// <summary>Increases maximum capacity</summary>
    public void IncreaseMaxCapacity()
    {
        MaxCapacity++;
    }


    /// <summary>Handles the elevator part of an ElevatorMovement</summary>
    /// <param name="movementAction">The movement I should handle</param>
    public void HandleMovementAction(ElevatorMovementAction movementAction)
    {
        handleActionChange(movementAction.Action);
        handlePassengerMovement(movementAction.GetsIn, movementAction.GetsOut);
    }

    /// <summary>Changes my LastAction property according to the ElevatorMovement.Action</summary>
    /// <param name="action"></param>
    private void handleActionChange(ElevatorMovementAction.ActionTypes action)
    {
        switch (action)
        {
            case ElevatorMovementAction.ActionTypes.GoUp:
                CurrentFloor++;
                break;
            case ElevatorMovementAction.ActionTypes.GoDown:
                CurrentFloor--;
                break;
        }
        addToHistory(action);
    }

    /// <summary>
    /// Add an action to the history
    /// </summary>
    /// <param name="action">The latest action</param>
    private void addToHistory(ElevatorMovementAction.ActionTypes action)
    {
        ElevatorMovementAction.ActionTypes temp = History[History.Length - 1];
        for (int i = History.Length - 1; i > 0; i--)
        {
            History[i] = History[i - 1];
        }
        History[0] = action;
    }

    /// <summary>Moves passengers in and out</summary>
    /// <param name="getsIn"></param>
    /// <param name="getsOut"></param>
    private void handlePassengerMovement(Passenger[] getsIn, Passenger[] getsOut)
    {
        PassengersInside = withRemovedPassengers(PassengersInside, getsOut);
        PassengersInside = withAddedPassengers(PassengersInside, getsIn);

        foreach (Passenger passenger in getsIn)
            passenger.MoveInsideElevator();

        foreach (Passenger passenger in getsOut)
            passenger.Arrive();
    }

    /// <summary>Executes set operation LEFT - RIGHT</summary>
    /// <param name="originalPassengers">LEFT set: Array of passengers to remove from</param>
    /// <param name="toRemove">RIGHT set: Array of passengers to remove</param>
    /// <returns>A new array with some removed</returns>
    private Passenger[] withRemovedPassengers(Passenger[] originalPassengers, Passenger[] toRemove)
    {
        List<Passenger> prevInsideMe = new List<Passenger>(originalPassengers);
        foreach (Passenger passenger in toRemove)
            prevInsideMe.Remove(passenger);
        return prevInsideMe.ToArray();
    }

    /// <summary>Executes set operation A + B</summary>
    /// <param name="originalPassengers">Set 1</param>
    /// <param name="toRemove">Set 2</param>
    /// <returns>A new array containing items from both arrays</returns>
    private Passenger[] withAddedPassengers(Passenger[] set1, Passenger[] set2)
    {
        List<Passenger> result = new List<Passenger>();
        foreach (Passenger passenger in set1)
            result.Add(passenger);
        foreach (Passenger passenger in set2)
            result.Add(passenger);

        return result.ToArray();
    }
}
