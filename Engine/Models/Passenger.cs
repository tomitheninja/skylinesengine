using System;

/// <summary>The Passenger model represents someone who uses or will use the elevator</summary>
public class Passenger : ObjectWithUniqueID
{
    /// <summary>Represents my destination.</summary>
    public readonly int TargetFloor;

    /// <summary>Represents my initial floor.</summary>
    public readonly int InitialFloor;

    /// <summary>Returns true if I am travelling</summary>
    /// <value>Only getter, use @MoveInsideElevator</value>
    public bool isInElevator { get; private set; }

    public bool isArrived { get; private set; }

    /// <summary></summary>
    /// <param name="TargetFloor">The floor I start on</param>
    /// <param name="InitialFloor">The floor I am going to</param>
    public Passenger(int TargetFloor, int InitialFloor)
    {
        validateFloor(TargetFloor);
        validateFloor(InitialFloor);
        this.TargetFloor = TargetFloor;
        this.InitialFloor = InitialFloor;
    }

    /// <summary>Moves me inside the elevator</summary>
    public void MoveInsideElevator()
    {
        if (isInElevator) throw new Exception("I am already inside an elevator");
        else isInElevator = true;
    }

    /// <summary>Mark me as arrived</summary>
    public void Arrive()
    {
        if (!isInElevator) throw new Exception("I couldn't arrive because I was not in an elevator");
        else
        {
            isInElevator = false;
            isArrived = true;
        }
    }

    /// <summary>Throws error if floor is invalid</summary>
    /// <param name="floor">The floor to validate</param>
    private static void validateFloor(int floor)
    {
        if (floor < 0) throw new Exception($"Floor should be positive. Got {floor}");
    }
}
