using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;

/// <summary>The is a base class</summary>
public abstract class ElevatorMovementAction
{
    public enum ActionTypes { Initial = 0, Stop, GoUp, GoDown }

    /// <summary>Array of passengers who moves in the elevator</summary>
    public Passenger[] GetsIn { get; private set; }

    /// <summary>Array of passengers who moves out from the elevator</summary>
    public Passenger[] GetsOut { get; private set; }

    /// <summary>The action for the elevator</summary>
    public ActionTypes Action { get; private set; }

    /// <summary></summary>
    /// <param name="elevator"></param>
    /// <param name="passengers"></param>
    protected ElevatorMovementAction(Elevator elevator, List<Passenger> passengers)
    {
        ActionTypes selectedAction = Worker(elevator, passengers);
        handleAction(selectedAction, elevator, passengers);
    }

    /// <summary>Handles the type of action (usually returned from derived class)</summary>
    /// <param name="action">Type of action</param>
    /// <param name="elevator">Reference to the elevator</param>
    /// <param name="passengers">Reference to the passengers</param>
    private void handleAction(ActionTypes action, Elevator elevator, List<Passenger> passengers)
    {
        switch (action)
        {
            case ActionTypes.GoUp:
                handleGoUpAction();
                break;
            case ActionTypes.GoDown:
                handleGoDownAction();
                break;
            case ActionTypes.Stop:
                handleOpenDoorAction(
                    elevator,
                    GetPassengersWhoWantsToGetIn(passengers, elevator.CurrentFloor),
                    GetPassengersWhoWantsToGetOut(passengers, elevator.CurrentFloor)
                );
                break;
            default:
                throw new Exception("Please implement me");
        }
    }

    /// <summary>Handle a GoUp action</summary>
    private void handleGoUpAction()
    {
        Action = ActionTypes.GoUp;
        GetsIn = new Passenger[0];
        GetsOut = new Passenger[0];
    }

    /// <summary>Handle a GoDown action</summary>
    private void handleGoDownAction()
    {
        Action = ActionTypes.GoDown;
        GetsIn = new Passenger[0];
        GetsOut = new Passenger[0];
    }

    /// <summary>Handle a Stop/OpenDoor action</summary>
    /// <param name="elevator"></param>
    /// <param name="PassengersWhoWantsToGetOut"></param>
    /// <param name="PassengersWhoWantsToGetIn"></param>
    private void handleOpenDoorAction(Elevator elevator, Passenger[] PassengersWhoWantsToGetIn, Passenger[] PassengersWhoWantsToGetOut)
    {
        Action = ActionTypes.Stop;
        GetsOut = PassengersWhoWantsToGetOut;

        int numRemainingSlots = GetNumRemainingSlots(elevator, PassengersWhoWantsToGetOut.Length);
        List<Passenger> PassengersWhoWantsToGetInRemaining = PassengersWhoWantsToGetIn.ToList();

        List<Passenger> getsIn = new List<Passenger>();
        while (numRemainingSlots > 0 && PassengersWhoWantsToGetInRemaining.Count > 0)
        {
            Passenger p = PassengersWhoWantsToGetInRemaining.First();
            PassengersWhoWantsToGetInRemaining.Remove(p);

            getsIn.Add(p);
            numRemainingSlots--;
        }
        GetsIn = getsIn.ToArray();
    }

    /// <summary></summary>
    /// <param name="elevator"></param>
    /// <param name="numMovesout">Number of passengers moving out</param>
    /// <returns>Number of remaining slots in the elevator, if some moves out</returns>
    protected int GetNumRemainingSlots(Elevator elevator, int numMovesout)
    {
        return elevator.MaxCapacity - elevator.NumPassengers + numMovesout;
    }

    /// <returns>Number of remaining slots in the elevator</returns>
    protected int GetNumRemainingSlots(Elevator elevator)
    {
        return elevator.MaxCapacity - elevator.NumPassengers;
    }

    protected virtual Passenger[] GetPassengersWhoWantsToGetIn(List<Passenger> passengers, int currentFloor)
    {
        return passengers.Where(p => !p.isInElevator && p.InitialFloor == currentFloor).ToArray();
    }

    protected virtual Passenger[] GetPassengersWhoWantsToGetOut(List<Passenger> passengers, int currentFloor)
    {
        return passengers.Where(p => p.isInElevator && p.TargetFloor == currentFloor).ToArray();
    }

    /// <summary>
    /// Implement this method in derived class
    /// 
    /// It should select the next action for the elevator
    /// </summary>
    /// <param name="elevator"></param>
    /// <param name="passengers"></param>
    /// <returns>The selected ActionTypes</returns>
    protected abstract ActionTypes Worker(Elevator elevator, List<Passenger> passengers);
}
