﻿/// <summary>The inherited classes will have a truly unique ID property</summary>
public abstract class ObjectWithUniqueID
{
    /// <summary>Stores the next availabe ID</summary>
    private static long nextID = 1;

    /// <summary>The unique identifier of the object</summary>
    public readonly long ID;

    public ObjectWithUniqueID()
    {
        ID = nextID++;
    }
}
