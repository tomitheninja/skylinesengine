﻿using System;
using System.Linq;
using System.Collections.Generic;

namespace SkylinesEngine
{
    class Program
    {
        static Elevator elevator;
        static List<Passenger> passengers;

        static void Main(string[] args)
        {
            elevator = new Elevator();
            passengers = new List<Passenger>();

            elevator.IncreaseMaxCapacity();
            elevator.AddFloor();

            passengers.Add(new Passenger(TargetFloor: 1, InitialFloor: 0));
            passengers.Add(new Passenger(TargetFloor: 1, InitialFloor: 0));
            passengers.Add(new Passenger(TargetFloor: 0, InitialFloor: 1));
            passengers.Add(new Passenger(TargetFloor: 0, InitialFloor: 1));

            ElevatorMovementAction action;
            action = new StopElevatorAction(elevator, passengers);
            elevator.HandleMovementAction(action);
            passengers = passengers.Where(p => !p.isArrived).ToList();

            action = new GoUpElevatorAction(elevator, passengers);
            elevator.HandleMovementAction(action);
            passengers = passengers.Where(p => !p.isArrived).ToList();

            action = new StopElevatorAction(elevator, passengers);
            elevator.HandleMovementAction(action);
            passengers = passengers.Where(p => !p.isArrived).ToList();

            action = new GoDownElevatorAction(elevator, passengers);
            elevator.HandleMovementAction(action);
            passengers = passengers.Where(p => !p.isArrived).ToList();

            action = new StopElevatorAction(elevator, passengers);
            elevator.HandleMovementAction(action);
            passengers = passengers.Where(p => !p.isArrived).ToList();
        }
    }
}
