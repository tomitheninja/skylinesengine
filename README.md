# SkyLines Engine

Simulates elevator movement

Subpart of project SkyLines

## Installation

1. Copy the Engine folder into \<your unity project\>/Assets/Scripts/

## Tests

Work In Progress.

See #6

## Dependencies

- [dotnet core v3.0](https://dotnet.microsoft.com/download)

### Development dependencies
- [Visual Studio Code 1.40](https://code.visualstudio.com/)

## Authors

- Team Yawapan Saya
